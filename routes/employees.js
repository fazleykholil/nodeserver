var chats = {
    "type": "FeatureCollection",
    "features": [
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": [
          [
            14.0625,
            27.68352808378776
          ],
          [
            86.1328125,
            11.523087506868514
          ],
          [
            98.0859375,
            55.37911044801047
          ],
          [
            149.765625,
            54.97761367069625
          ],
          [
            130.4296875,
            -47.04018214480665
          ],
          [
            51.67968749999999,
            -71.41317683396565
          ],
          [
            -15.468749999999998,
            -48.92249926375824
          ],
          [
            -23.203125,
            6.315298538330033
          ],
          [
            24.960937499999996,
            43.58039085560784
          ],
          [
            95.625,
            44.08758502824516
          ]
        ]
            }
    },
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "LineString",
                "coordinates": [
          [
            57.660638093948364,
            -20.42421269949928
          ],
          [
            57.661153078079224,
            -20.424423840988197
          ],
          [
            57.66175925731659,
            -20.424715416853694
          ],
          [
            57.66248881816864,
            -20.425162833917664
          ],
          [
            57.66392111778259,
            -20.426288911556316
          ],
          [
            57.66681790351868,
            -20.430240172351798
          ],
          [
            57.6676332950592,
            -20.431547182845303
          ],
          [
            57.67143130302429,
            -20.433155795897854
          ],
          [
            57.67402768135071,
            -20.434201385359426
          ],
          [
            57.67381310462951,
            -20.434523103763546
          ],
          [
            57.67385601997375,
            -20.434784499471306
          ],
          [
            57.673126459121704,
            -20.436483560741063
          ],
          [
            57.67181754112244,
            -20.43846409958867
          ],
          [
            57.67077684402465,
            -20.439147731440542
          ],
          [
            57.66915678977966,
            -20.4382730842632
          ],
          [
            57.66298770904541,
            -20.434513050073605
          ],
          [
            57.66033768653869,
            -20.43292455880702
          ],
          [
            57.66091704368591,
            -20.43205993008256
          ],
          [
            57.66351342201233,
            -20.43190912224904
          ],
          [
            57.66540169715881,
            -20.43077303181705
          ],
          [
            57.665873765945435,
            -20.43027033425761
          ]
        ]
            }
    },
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Point",
                "coordinates": [
          57.663159370422356,
          -20.429667095017628
        ]
            }
    },
        {
            "type": "Feature",
            "properties": {
                "marker-color": "#40fd00",
                "marker-size": "medium",
                "marker-symbol": "museum",
                "name": "naisa"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
          57.665605545043945,
          -20.426550321257956
        ]
            }
    },
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Point",
                "coordinates": [
          57.67006874084472,
          -20.429988822906697
        ]
            }
    },
        {
            "type": "Feature",
            "properties": {},
            "geometry": {
                "type": "Point",
                "coordinates": [
          57.66412496566772,
          -20.43489508982929
        ]
            }
    }
  ]
};

exports.findAll = function (req, res, next) {
    var name = req.query.name;
    console.log('name: ' + name);
    if (name) {
        var results = chats.filter(function (element) {
            var fullName = element.name + "," + element.message;
            return fullName.toLowerCase().indexOf(name.toLowerCase()) > -1;
        });
        res.send(results);
    } else {
        res.send(chats);
    }
};

exports.findById = function (req, res, next) {
    var id = req.params.id;
    res.send(chats[id]);
};
